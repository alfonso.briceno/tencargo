import OrderServiceController from "../../src/OrderRequest/OrderServiceFacade/OrderServiceController";
import OrderServiceFacadeImp from "../../src/OrderRequest/OrderServiceFacade/OrderServiceFacadeImp";
import order from "../fixtures/Order.json";
describe("Order Request Test", () => {
  it("Request Order!", () => {
    let controller = new OrderServiceController();
    controller.facade = new OrderServiceFacadeImp();
    controller.orderProduct(order);
    const result = controller.orderFulfilled;

    expect(result).to.equal(true);
  });
});
