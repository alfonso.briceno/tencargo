import carrierRequests from "../fixtures/CarrierRequest";

import state from "../../src/OrderFulfillment/ObserverPattern/State";

import RouteObserver from "../../src/OrderFulfillment/ObserverPattern/RouteObserver";
describe("Order Fulfillment Test", () => {
  it("Fulfill Order!", () => {
    // Instantiate classes.
    new RouteObserver();

    new RouteObserver();

    state.update(carrierRequests[0]);

    state.update(carrierRequests[1]);

    //expect(result).to.equal(true);
  });
});
