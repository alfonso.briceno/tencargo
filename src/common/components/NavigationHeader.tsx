import React from "react";

const NavigationHeader = ({ history }: any) => {
  console.log("Navigaton Header");
  return (
    <div>
      <button onClick={() => history.push("/")}>Carrier Requests</button>
      <button onClick={() => history.push("/jobs")}>Transportation Jobs</button>
    </div>
  );
};

export default NavigationHeader;
