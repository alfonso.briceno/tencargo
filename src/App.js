import * as React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import JobListing from "./OrderFulfillment/components/JobListing";
import CarrierListing from "./OrderFulfillment/components/CarrierListing";
import NavigationHeader from "./common/components/NavigationHeader";

export const SCREENS = {
  Jobs: {
    title: "Transportation Jobs",
    route: "/",
    component: JobListing,
  },
  CarrierRequests: {
    route: "/carrier",
    component: CarrierListing,
  },
};

function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to={SCREENS.CarrierRequests.route}>Carrier Requests</Link>
            </li>
            <li>
              <Link to={SCREENS.Jobs.route}>Transportation Jobs</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path={SCREENS.CarrierRequests.route}>
            <CarrierListing></CarrierListing>
          </Route>
          <Route path={SCREENS.Jobs.route}>
            <JobListing></JobListing>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
