type Order = {
  id: number;
  products: Product[];
  paymentMethod: PaymentMethod;
  client: Client;
};

export enum PaymentMethod {
  "BankTransfer" = "Bank Transfer",
  "Cash" = "Cash",
}

export type Client = {
  id: number;
  name: string;
};

export type Product = {
  id: number;
  name: string;
  quantity: number;
};
export default Order;
