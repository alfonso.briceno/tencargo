import { OrderServiceFacade } from "./OrderServiceFacade";
import Order from "../types/Order";

export default class OrderFulfillmentController {
  facade!: OrderServiceFacade;
  orderFulfilled: boolean = false;
  orderProduct(order: Order) {
    this.orderFulfilled = this.facade.placeOrder(order);
    console.log("Order Flow Completed.");
  }
}
