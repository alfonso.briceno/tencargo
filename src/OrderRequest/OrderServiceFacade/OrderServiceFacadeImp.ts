import InventoryService from "../Services/InventoryService";
import PaymentService from "../Services/PaymentService";
import DeliveryService from "../Services/DeliveryService";
import ProductionService from "../Services/ProductionService";
import ClientSupportService from "../Services/ClientSupportService";
import { OrderServiceFacade } from "./OrderServiceFacade";

import NotVerified from "../OrderStates/NotVerified";

import orderContext from "../OrderStates/Context";
import PackagingService from "../Services/PackagingService";
import Order from "../types/Order";
export default class OrderServiceFacadeImpl implements OrderServiceFacade {
  placeOrder(order: Order) {
    let orderFulfilled: boolean = false;

    let unverified: NotVerified = new NotVerified();
    unverified.handle(orderContext);

    order.products.map((product) => {
      if (!InventoryService.isAvailable(product.id)) {
        if (ProductionService.buildProduct(product.id)) {
          ClientSupportService.notifyLateArrival(order.client);
        } else {
          order.products.filter((pr) => pr.id === product.id);
          ClientSupportService.notifyUnavailable(order.client, product);
          InventoryService.removeProductFromCatalog(product.id);
        }
      }
    });

    let paymentConfirmed = PaymentService.makePayment(order.paymentMethod);
    if (paymentConfirmed) {
      PackagingService.packProducts(order);
      DeliveryService.shipProducts(order);
      DeliveryService.orderDelivered(order.id);
      orderFulfilled = true;
    }

    return orderFulfilled;
  }
}
