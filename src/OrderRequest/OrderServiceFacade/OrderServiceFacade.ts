import Order from "../types/Order";

export interface OrderServiceFacade {
  placeOrder(order: Order): boolean;
}
