import Verified from "../OrderStates/Verified";
import orderContext from "../OrderStates/Context";
import { Product } from "../types/Order";
export default class InventoryService {
  static isAvailable(productId: number) {
    /*Check Warehouse database for product availability*/

    console.log("Producto con ID: " + productId + " está disponible.");
    return true;
  }
  static removeProductFromCatalog(productId: number) {
    console.log(
      "Product with id of: " + productId + " has been removed from the catalog."
    );
    return true;
  }
}
