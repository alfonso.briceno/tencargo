import orderContext from "../OrderStates/Context";
import Preparing from "../OrderStates/Preparing";
import Order from "../types/Order";
export default class PackagingService {
  static packProducts(order: Order) {
    let preparing: Preparing = new Preparing();
    preparing.handle(orderContext);
    console.log("Packing products for order Id:" + order.id);
    /*Check Warehouse database for product availability*/
  }
}
