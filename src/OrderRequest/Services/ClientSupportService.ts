import Delay from "../OrderStates/Delay";
import orderContext from "../OrderStates/Context";
import { Product, Client } from "../types/Order";
export default class ClientSupportService {
  static notifyLateArrival(client: Client) {
    let delay: Delay = new Delay();
    delay.handle(orderContext);
    console.log("Client " + client.name + " has been notified of late arrival");
    /*Check Warehouse database for product availability*/
  }
  static notifyUnavailable(client: Client, product: Product) {
    console.log(
      "Client " +
        client.name +
        " has been notified of unavailability of product " +
        product.name
    );
  }
}
