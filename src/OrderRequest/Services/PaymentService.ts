import { PaymentMethod } from "../types/Order";
import WaitingTransfer from "../OrderStates/WaitingTransfer";
import orderContext from "../OrderStates/Context";
import Verified from "../OrderStates/Verified";

export default class PaymentService {
  static makePayment(paymentMethod: PaymentMethod) {
    /*Connect with payment gateway for payment*/

    if (paymentMethod === PaymentMethod.BankTransfer) {
      let waitingTransfer: WaitingTransfer = new WaitingTransfer();
      waitingTransfer.handle(orderContext);
      console.log("Waiting for bank transfer");
      return false;
    }
    let verified: Verified = new Verified();
    verified.handle(orderContext);
    console.log("Payment Confirmed");
    return true;
  }
}
