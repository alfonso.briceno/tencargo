import Transporting from "../OrderStates/Transporting";
import orderContext from "../OrderStates/Context";
import Delivered from "../OrderStates/Delivered";
import Order from "../types/Order";
export default class DeliveryService {
  static shipProducts(order: Order) {
    let transporting: Transporting = new Transporting();
    transporting.handle(orderContext);
    console.log("Shipping products for order ID:" + order.id);
  }

  static orderDelivered(orderId: number) {
    let delivered: Delivered = new Delivered();
    delivered.handle(orderContext);
    console.log("Order with Id:" + orderId + " is delivered");
  }
}
