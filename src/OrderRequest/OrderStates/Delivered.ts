import { Context } from "./Context";
import OrderState from "./OrderState";

class Delivered implements OrderState {
  handle(context: Context): void {
    context.setState(this);
    console.log("Order is on Delivered state");
  }
}
export default Delivered;
