import OrderState from "./OrderState";
import { Context } from "./Context";

class NotVerified implements OrderState {
  handle(context: Context): void {
    context.setState(this);
    console.log("Order is on Not Verified state");
  }
}
export default NotVerified;
