import Context from "./Context";
import OrderState from "./OrderState";

class Preparing implements OrderState {
  handle(context: typeof Context): void {
    context.setState(this);
    console.log("Order is on Preparing state");
  }
}
export default Preparing;
