import { Context } from "./Context";
import OrderState from "./OrderState";

class Verified implements OrderState {
  handle(context: Context): void {
    context.setState(this);
    console.log("Order is on Verified state");
  }
}
export default Verified;
