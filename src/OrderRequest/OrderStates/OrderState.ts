import { Context } from "./Context";

export default interface OrderState {
  handle(context: Context): void;
}
