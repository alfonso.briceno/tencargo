import { Context } from "./Context";
import OrderState from "./OrderState";

class WaitingTransfer implements OrderState {
  handle(context: Context): void {
    context.setState(this);
    console.log("Order is on Waiting Transfer state");
  }
}
export default WaitingTransfer;
