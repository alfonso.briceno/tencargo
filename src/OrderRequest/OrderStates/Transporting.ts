import { Context } from "./Context";
import OrderState from "./OrderState";

class Transporting implements OrderState {
  handle(context: Context): void {
    context.setState(this);
    console.log("Order is on Transporting state");
  }
}
export default Transporting;
