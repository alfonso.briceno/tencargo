import Context from "./Context";
import OrderState from "./OrderState";

class Delay implements OrderState {
  handle(context: typeof Context): void {
    context.setState(this);
    console.log("Order is on Delay state");
  }
}
export default Delay;
