import OrderState from "./OrderState";

export class Context {
  state: null | OrderState;

  constructor() {
    this.state = null;
  }

  setState(state: OrderState) {
    this.state = state;
  }

  getState() {
    return this.state;
  }
}

let orderContext = new Context();
export default orderContext;
