import { CSubject } from "./Subject";

class State extends CSubject {
  state: Object[];
  constructor() {
    super();
    this.state = [];
  }

  // Update the state.
  // Calls the update method on each observer.
  update(data = {}) {
    this.state.push(data);
    this.notify(this.state);
  }

  // Get the state.
  get() {
    return this.state;
  }
}
const state = new State();
export default state;
