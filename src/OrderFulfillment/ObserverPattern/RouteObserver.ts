import state from "./State";

class RouteObserver {
  observerId: number;

  constructor() {
    this.observerId = state.observers.length++;

    state.addObserver(this);
  }

  update(data: any) {
    console.log(
      "Calculating new routes and weight for RouteObserver" + this.observerId,
      data
    );
  }
}

export default RouteObserver;
