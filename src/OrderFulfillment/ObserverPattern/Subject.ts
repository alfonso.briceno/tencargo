export class CSubject {
  observers: any;
  constructor() {
    this.observers = [];
  }

  addObserver(observer: any) {
    this.observers.push(observer);
  }

  removeObserver(observer: any) {
    const removeIndex = this.observers.findIndex((obs: any) => {
      return observer === obs;
    });

    console.log("Observer " + removeIndex + " deleted.");
    if (removeIndex !== -1) {
      this.observers = this.observers.slice(removeIndex, 1);
    }
  }
  notify(data: any) {
    console.log("Carrier request updated");
    if (this.observers.length > 0) {
      this.observers.forEach((observer: any) => observer.update(data));
    }
  }
}
const Subject = new CSubject();
export default Subject;
