import Receiver from "../types/Receiver";
import Sender from "../types/Sender";
import Location from "../types/Location";
import Package from "../types/Package";

export interface CarrierRequestProps {
  id: number;
  package: Package;
  receiver: Receiver;
  sender: Sender;
  location: Location;
}

export default class CarrierRequest {
  package: Package;
  receiver: Receiver;
  sender: Sender;
  location: Location;

  constructor(props: CarrierRequestProps) {
    this.package = props.package;
    this.receiver = props.receiver;
    this.sender = props.sender;
    this.location = props.location;
  }
}
