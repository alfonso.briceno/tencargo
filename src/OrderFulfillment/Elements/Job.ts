import Route from "../types/Route";

import Report from "./Report";

interface JobProps {
  weight: number;
  route: Route;
}

export default class Job extends Report {
  weight: number;
  route: Route;
  constructor(props: JobProps) {
    super();
    this.weight = props.weight;
    this.route = props.route;
  }
}
