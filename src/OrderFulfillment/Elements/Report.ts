export default class Report {
  constructor() {}
  createReport() {
    console.log("Report Created");
  }
  notifyReceiver() {
    console.log("Receiver notified");
  }
  notifySender() {
    console.log("Sender notified");
  }
}
