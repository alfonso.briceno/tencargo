type Package = {
  id: string;
  weight: number;
};

export default Package;
