import React, { useState, useEffect } from "react";

import generateJobs from "../utils/generateJobs";
import { CarrierRequestProps } from "../Elements/CarrierRequest";
import state from "../ObserverPattern/State";
import RouteObserver from "../ObserverPattern/RouteObserver";

export const JobListing = () => {
  const [jobListing, setJobListing] = useState<any>(null);

  useEffect(() => {
    let routeObserver = new RouteObserver();

    return () => state.removeObserver(routeObserver);
  }, []);

  return <span> Lista de trabajos</span>;
};

export default JobListing;

/* {jobListing.map((job: React.ReactNode) => (
   <span> {job}</span>
 ))}*/
