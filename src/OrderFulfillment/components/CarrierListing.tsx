import React, { useState, useEffect } from "react";

import state from "../ObserverPattern/State";
import generateRandomKey from "../utils/generateRandomKey";
export const CarrierListing = () => {
  const [carrierRequestId, setCarrierRequestId] = useState();

  const onChange = (e: any) => {
    setCarrierRequestId(e.target.value);
  };
  function saveRequest() {
    let id = generateRandomKey(6);

    // Updating state will prompt a re-render via the notify method being called.
    state.update({ id: carrierRequestId });
  }

  return (
    <div>
      <input
        onChange={(e) => onChange(e)}
        value={carrierRequestId}
        type="text"
      />
      <button
        onClick={() => saveRequest()}
        className="ui primary button"
        type="submit"
      >
        Guardar
      </button>
    </div>
  );
};

export default CarrierListing;
