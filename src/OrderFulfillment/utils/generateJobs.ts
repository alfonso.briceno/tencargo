import { CarrierRequestProps } from "../Elements/CarrierRequest";

export default (carrierRequests: CarrierRequestProps[]) => {
  const jobsRoutes = calculateRoute(carrierRequests);
  const jobsWeight = calculateWeight(jobsRoutes);

  return jobsWeight;
};

function calculateRoute(carrierRequests: CarrierRequestProps[]) {
  const carrierSorted = carrierRequests.sort(
    (a, b) => a.location.latitude - b.location.latitude
  );

  console.log("New route calculated for jobs.");
  return carrierSorted;
}

function calculateWeight(jobsRoutes: CarrierRequestProps[]) {
  console.log("New weight calculated for each job.");
  return jobsRoutes;
}
